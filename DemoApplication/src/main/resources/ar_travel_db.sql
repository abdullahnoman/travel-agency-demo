/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.27-0ubuntu0.16.04.1 : Database - ar_travel_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ar_travel_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ar_travel_db`;

/*Table structure for table `location` */

DROP TABLE IF EXISTS `location`;

CREATE TABLE `location` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `location` */

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `role` */

insert  into `role`(`Id`,`name`) values (1,'ADMIN');

/*Table structure for table `status` */

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `status` varchar(500) DEFAULT NULL,
  `privacy` enum('PRIVATE','PUBLIC') DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `status` */

insert  into `status`(`Id`,`status`,`privacy`,`location_id`) values (1,'data message','PUBLIC',NULL),(2,'status messges','PUBLIC',NULL),(3,'dfasdf sdfasf afsadfa sf fasdf a','PUBLIC',NULL),(4,'sdfs fasdfas asdfa sdfas sdf a','PUBLIC',NULL),(5,'sfasfa sdfa \r\nsdfasdf\r\n','PUBLIC',NULL),(6,'sfasfa sdfa \r\nsdfasdf\r\n','PUBLIC',NULL),(7,'fsfasf fasdf asf','PUBLIC',NULL),(8,'dfasfd as sdf asfd \r\nsdfasf','PUBLIC',NULL),(9,'fsfsadfasdf\r\nsdfsadf\r\nsdfasdf\r\nsdfas','PUBLIC',NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `role_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`Id`,`username`,`password`,`email`,`first_name`,`middle_name`,`last_name`,`role_id`) values (1,'test','$2a$10$Lga3qrX8ZIATaUWzttv7nexFpfWjQYVbuZob2.mz0IlrSyL5q5MAy','abc@gmail.com','afa','fdsf','asdfas)',1),(2,'abdullah','$2a$10$MbrXgDAUEGJGS6f8T5ivye8F8pJQEmseB6QCP8e8J1sjElM/Av4mm','noman@gmail.com','Abdullah',NULL,'Al Noman',1),(3,'jhon','$2a$10$.ojqW.a2nKnHCXNHnlzohuIhLLReYEMtlVeR4xRHT7.EwNZY2usYW','jhon@gmail.com','Jhon',NULL,'diggle',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
