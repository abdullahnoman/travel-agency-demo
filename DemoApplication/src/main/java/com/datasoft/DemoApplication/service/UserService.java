package com.datasoft.DemoApplication.service;

import com.datasoft.DemoApplication.model.Role;
import com.datasoft.DemoApplication.model.User;
import com.datasoft.DemoApplication.repository.RoleRepository;
import com.datasoft.DemoApplication.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    public boolean isRegistered(User user) {
        try{
            if(userRepository.checkUserNameExists(user.getUsername().trim())){
                return Boolean.FALSE;
            }else if(userRepository.checkUserEmailExists(user.getEmail().trim())){
                return Boolean.FALSE;
            }else{
                user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
                Role role = new Role();
                role.setId(1);
                user.setRole(role);
                User savedUser =  userRepository.add(user);
                if(savedUser.getId() !=null){
                    return Boolean.TRUE;
                }else{
                    return Boolean.FALSE;
                }
            }

        }catch(Exception ex){
            log.error("Error Found In Registration:{}",ex.getLocalizedMessage());
            return Boolean.FALSE;
        }
    }
}