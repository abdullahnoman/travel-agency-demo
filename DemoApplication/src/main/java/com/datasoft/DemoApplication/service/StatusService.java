package com.datasoft.DemoApplication.service;

import com.datasoft.DemoApplication.model.Location;
import com.datasoft.DemoApplication.model.Status;
import com.datasoft.DemoApplication.model.enums.PrivacyPolicy;
import com.datasoft.DemoApplication.repository.LocationRepository;
import com.datasoft.DemoApplication.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by noman on 7/27/19.
 */
@Service
public class StatusService {

    @Autowired
    private StatusRepository statusRepository;

    public boolean add(Status status) {

        // TODO: 7/27/19 Update the location value from the form field
        Location location = new Location();
        location.setName("Dhaka");
        status.setLocation(location);
        // TODO: 7/27/19 Update this code, for setting data from the form
        status.setPrivacyPolicy(PrivacyPolicy.PUBLIC);

        if(statusRepository.add(status).getId() != null){
            return Boolean.TRUE;
        }else{
            return Boolean.FALSE;
        }
    }
}
