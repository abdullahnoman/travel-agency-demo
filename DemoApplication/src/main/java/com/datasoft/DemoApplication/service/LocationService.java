package com.datasoft.DemoApplication.service;

import com.datasoft.DemoApplication.model.Location;
import com.datasoft.DemoApplication.model.enums.PrivacyPolicy;
import com.datasoft.DemoApplication.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by noman on 7/27/19.
 */
@Repository
public class LocationService {

    @Autowired
    private LocationRepository locationRepository;

    public boolean add(Location location) {
        if(locationRepository.add(location).getId() != null){
            return Boolean.TRUE;
        }else{
            return Boolean.FALSE;
        }
    }
}
