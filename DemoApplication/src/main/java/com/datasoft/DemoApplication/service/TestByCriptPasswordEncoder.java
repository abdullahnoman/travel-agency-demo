package com.datasoft.DemoApplication.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class TestByCriptPasswordEncoder {

    public static void main(String[] args) {
        String password = "password";
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        passwordEncoder.matches(password,passwordEncoder.encode(password));
        System.out.println(passwordEncoder.encode(password));
    }
}