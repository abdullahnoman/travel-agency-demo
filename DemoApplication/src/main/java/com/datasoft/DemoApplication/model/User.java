package com.datasoft.DemoApplication.model;

import lombok.Data;

@Data
public class User {
    private Integer Id;
    private String username;
    private String password;
    private String email;
    private String firstName;
    private String middleName;
    private String lastName;
    private Role role;

}