package com.datasoft.DemoApplication.model;

import lombok.Data;

/**
 * Created by noman on 7/27/19.
 */
@Data
public class Location {
    private Integer id;
    private String name;
}
