package com.datasoft.DemoApplication.model;

import com.datasoft.DemoApplication.model.enums.PrivacyPolicy;
import lombok.Data;

/**
 * Created by noman on 7/27/19.
 */
@Data
public class Status {
   private Integer id;
   private String status;
   private PrivacyPolicy privacyPolicy;
   private Location location;
}
