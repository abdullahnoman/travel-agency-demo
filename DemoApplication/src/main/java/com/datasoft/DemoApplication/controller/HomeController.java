package com.datasoft.DemoApplication.controller;


import com.datasoft.DemoApplication.model.User;
import com.datasoft.DemoApplication.repository.UserRepository;
import com.datasoft.DemoApplication.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@Controller
public class HomeController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        return "login/login";
    }

    @RequestMapping(value = "/userData", method = RequestMethod.GET)
    public String userData(Model model) {
        List<User> userList = userRepository.findAll();
        System.out.println(userList.toString());
        return "login/login";
    }

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String dashboard(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return "home/index";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register(Model model){
        return "register/register";
    }

    @PostMapping(value = "/register" )
    public String register(@ModelAttribute("register") User user, HttpServletRequest request , RedirectAttributes redirectAttributes){
        if(!user.getPassword().equals(request.getParameter("password_confirmation"))){
            redirectAttributes.addFlashAttribute("error","Password miss match found");
            return "redirect:register";
        }
        if(userService.isRegistered(user)){
            redirectAttributes.addFlashAttribute("success","You are successfully registered");
            return "redirect:login";
        }else{
            redirectAttributes.addFlashAttribute("error","Sorry !! Registration Failed");
        }
        return "redirect:register";
    }

    @GetMapping(value = "/forgetPassword")
    public String forgetPassword(Model model){
        return "register/forgot-password";
    }

    @GetMapping(value = "/403")
    public String error(Model model){
        model.addAttribute("title","ACCESS DENIED");
        return "error/403";
    }
}