package com.datasoft.DemoApplication.controller;

import com.datasoft.DemoApplication.model.Status;
import com.datasoft.DemoApplication.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Created by noman on 7/27/19.
 */
@Controller
public class StatusController {

    @Autowired
    private StatusService statusService;

    @PostMapping(value = "/status")
    public String add(@ModelAttribute("status") Status status,RedirectAttributes redirectAttributes) {
        if (statusService.add(status)) {
            redirectAttributes.addFlashAttribute("success", "Data Added");
        } else {
            redirectAttributes.addFlashAttribute("error", "Data is not added");
        }
        return "home/index";
    }
}
