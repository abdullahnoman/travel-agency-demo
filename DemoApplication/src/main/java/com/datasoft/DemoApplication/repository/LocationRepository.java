package com.datasoft.DemoApplication.repository;

import com.datasoft.DemoApplication.model.Location;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by noman on 7/27/19.
 */
@Repository
@Slf4j
public class LocationRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Location add(Location location){
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("location")
                .usingGeneratedKeyColumns("Id");
        Map<String,Object> parameterMap = new HashMap<String,Object>();
        parameterMap.put("name",location.getName());
        try{
            Number autoGenId = jdbcInsert.executeAndReturnKey(parameterMap);
            if(autoGenId !=null){
                location.setId(autoGenId.intValue());
                log.info("Location Added With ID {}",autoGenId);
                return location;
            }

        }catch (DataAccessException dae){
            log.error(dae.getMessage());
            log.info("Location Data did not added: {}",dae.getLocalizedMessage());
            return location;
        }
        return location;
    }

    public List<Location> findAll(){
        String query = "Select * from location";

        try{
            return  jdbcTemplate.query(query, new LocationRowMapper());
        }catch (DataAccessException dae){
            log.error("Error Found in Retrieving User Data: {}",dae.getLocalizedMessage());

        }
        return new ArrayList<>();
    }

    class LocationRowMapper implements RowMapper<Location>{
        @Override
        public Location mapRow(ResultSet resultSet, int i) throws SQLException {
            Location location = new Location();
            location.setId(resultSet.getInt("Id"));
            location.setName(resultSet.getString("name"));
            return location;
        }
    }
}
