package com.datasoft.DemoApplication.repository;

import com.datasoft.DemoApplication.model.Role;
import com.datasoft.DemoApplication.model.User;
import com.sun.istack.internal.Nullable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
@Repository
public class UserRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public User add(User user) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("user")
                .usingGeneratedKeyColumns("id");
        if(user.getUsername() == null){
            return user;
        }
        Map<String,Object> parameterMap = new HashMap<>();
        parameterMap.put("username",user.getUsername());
        parameterMap.put("email",user.getEmail());
        parameterMap.put("password",user.getPassword());
        parameterMap.put("first_name",user.getFirstName());
        parameterMap.put("last_name",user.getLastName());
        parameterMap.put("role_id",user.getRole().getId());
        try{
            Number autoGenId = jdbcInsert.executeAndReturnKey(parameterMap);
            if(autoGenId !=null){
                user.setId(autoGenId.intValue());
                log.info("User Added With ID {}",autoGenId);
                return user;
            }

        }catch (DataAccessException dae){
            log.error(dae.getMessage());
            log.info("User did not added: {}",dae.getLocalizedMessage());
            return user;
        }

        return user;
    }

    public List<User> findAll(){
        String query = "Select * from user";

        try{
           return  jdbcTemplate.query(query,new UserRowMapper());
        }catch (DataAccessException dae){
            log.error("Error Found in Retrieving User Data: {}",dae.getLocalizedMessage());

        }
        return new ArrayList<>();
    }

    public User findUserByUserName(String userName) {
        String query ="SELECT * FROM user WHERE BINARY username = ? ";
        try{
            return jdbcTemplate.queryForObject(query,new Object[]{userName},new UserRowMapper());
        }catch (DataAccessException dae){
           log.error("Error Found in Retrieving UserName, Error: {}",dae.getLocalizedMessage());
            return new User();
        }
    }
    public Boolean checkUserNameExists(String userName) {
        try {
            return jdbcTemplate.queryForObject("SELECT COUNT(id) FROM user WHERE username = ?",
                    new Object[]{userName}, Integer.class) == 1;
        } catch (DataAccessException e) {
            log.error("Error Found, {}",e.getMessage());
            return Boolean.FALSE;
        }
    }
    public Boolean checkUserEmailExists(String email) {
        try {
            return jdbcTemplate.queryForObject("SELECT COUNT(id) FROM user WHERE email = ?",
                    new Object[]{email}, Integer.class) == 1;
        } catch (DataAccessException e) {
            log.error("Error Found: {}",e.getMessage());
            return Boolean.FALSE;
        }
    }


    class UserRowMapper implements RowMapper<User>{
        @Nullable
        @Override
        public User mapRow(ResultSet resultSet, int i) throws SQLException {
            User user = new User();
            user.setId(resultSet.getInt("Id"));
            user.setUsername(resultSet.getString("username"));
            user.setPassword(resultSet.getString("password"));
            user.setEmail(resultSet.getString("email"));
            user.setFirstName(resultSet.getString("first_name"));
            user.setMiddleName(resultSet.getString("middle_name"));
            user.setLastName(resultSet.getString("last_name"));
            Role role = new Role();
            role.setId(resultSet.getInt("role_id"));
            user.setRole(role);
            return user;
        }
    }

}