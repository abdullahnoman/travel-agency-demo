package com.datasoft.DemoApplication.repository;

import com.datasoft.DemoApplication.model.Location;
import com.datasoft.DemoApplication.model.Status;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by noman on 7/27/19.
 */
@Repository
@Slf4j
public class StatusRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Status add(Status status){
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("status")
                .usingGeneratedKeyColumns("Id");
        Map<String,Object> parameterMap = new HashMap<String,Object>();
        parameterMap.put("status",status.getStatus());
        parameterMap.put("privacy",status.getPrivacyPolicy().name());
        parameterMap.put("location_id",status.getLocation().getId());

        try{
            Number autoGenId = jdbcInsert.executeAndReturnKey(parameterMap);
            if(autoGenId !=null){
                status.setId(autoGenId.intValue());
                log.info("Status Added With ID {}",autoGenId);
                return status;
            }

        }catch (DataAccessException dae){
            log.error(dae.getMessage());
            log.info("Status did not added: {}",dae.getLocalizedMessage());
            return status;
        }
        return status;
    }

}
